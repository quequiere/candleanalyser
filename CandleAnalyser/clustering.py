
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D

import matplotlib.pyplot as plt
import numpy as np
import random
import json
from mpl_finance import candlestick_ohlc

if __name__ == '__main__':
    print("Start clustering")


    with open('ChartData.json') as f:
        tempdata = json.load(f)
    data = tempdata["content"]["panes"][0]["sources"][0]["bars"]["data"]

    priceData = []

    candleId = 1
    for candle in data:
        v = candle["value"]

        open = v[1]
        close = v[4]
        min = v[3]
        max = v[2]

        if candleId < 100:
            dataToAppend = candleId, open, max, min,close , 1
            priceData.append(dataToAppend)

        xdata = close/open
        ydata = min/open
        zdata = max/open

        if candleId ==1 :
            X = np.array([[xdata, ydata, zdata]])
            first = False
        else :
            X = np.append(X,[[xdata, ydata, zdata]], axis=0)

        candleId = candleId + 1


    #clustering part
   
    kmeans = KMeans(n_clusters=4, random_state=0).fit(X)
    labels = kmeans.labels_


    #prediction part

    eto = 9.18
    etc = 9.16
    eth = 9.24
    etb = 9.10

    eto2 = 17.54
    etc2 = 17.54
    eth2 = 17.6
    etb2 = 17.5

    mo = 12.6
    mh = 12.6
    mb = 11.96
    mc = 12.30

    mo2 = 16.2
    mh2 = 16.26
    mb2 = 15.76
    mc2 = 16


    pred = kmeans.predict([
        [etc/eto, etb/eto,eth/eto], 
        [etc2/eto2, etb2/eto2,eth2/eto2], 
        [mc/mo, mb/mo,mh/mo], 
        [mc2/mo2, mb2/mo2,mh2/mo2] 
        ])
    print(pred)


    #plot part

    fig = plt.figure(1, figsize=(4, 3))
    ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)

    LABEL_COLOR_MAP = {0 : 'r', 1 : 'k', 2 : 'b', 3 : 'c', 4 : 'g', 5 : 'y', 6 : 'm',7 : 'm',8 : 'm',9 : 'm'}
    label_color = [LABEL_COLOR_MAP[l] for l in labels]
    ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=label_color, cmap='Set2', s=20)

    plt.show()


    #Display clustering
    LABEL_MARKER = {0 : '^', 1 : 'o', 2 : 's', 3 : 'p', 4 : '*', 5 : 'd', 6 : '|', 7 : '<', 8 : 'x', 9 : '+'}

    fig = plt.figure()
    ax1 = plt.subplot2grid((1,1), (0,0))
    candlestick_ohlc(ax1, priceData)
   
    id = 1
    for c in priceData:
        cluster = labels[id]
        localMarker = LABEL_MARKER[cluster]
        markerHigh = c[2]

        plt.plot(id,markerHigh, marker=localMarker, color='b')
        id = id +1
    
    plt.show()
